#include<stdio.h>

#define RENGS 255
#define COLS 255

// prototipo de funcion implementada al final de este archivo
void ppm_volcado(int r, int c, int R[][c], int G[][c], int B[][c]);

// implementacion de funcion antes de main no requiere prototipo
void ppm_blanco(int r, int c, int R[][c], int G[][c], int B[][c]){
	// se puede dejar la primera dimension sin especificar
	int i,j;
	for(i=0;i<r;i++){
		for(j=0;j<c;j++){
			R[i][j]=255 ;
			G[i][j]=255 ;
			B[i][j]=255 ;
		}
	}
}


void ppm_ejes(int r, int c, int R[][c], int G[][c], int B[][c]){
		int i;
		for(i=0;i<r;i++){
				R[i][50]=20 ;
				G[i][50]=50 ;
				B[i][50]=20 ;
				//
				R[50][i]=20 ;
				G[50][i]=50 ;
				B[50][i]=20 ;
		}
}


int main(){
	int R[100][100];
	int G[100][100];
	int B[100][100];
	int ancho=100,alto=100,i,j,x,y,z;
	ppm_blanco(alto, ancho,R,G,B);
	ppm_ejes(alto,ancho,R,G,B);

	for(x=-48;x<=46;x++){
		y=x+2;
		j=x+48;
		i=-y+48;
		R[i][j]=0 ;
		G[i][j]=0 ;
		B[i][j]=0 ;
	}
	for(x=-7;x<=6;x++){
		y=(x*x)+x-6;
		j=x+48;
		i=-y+48;
		R[i][j]=0 ;
		G[i][j]=0 ;
		B[i][j]=0 ;
	}
	ppm_volcado(alto,ancho,R,G,B);
	return 0;
}

void ppm_volcado(int r, int c, int R[][c], int G[][c], int B[][c]){
	int i,j;
	printf("P3\n%d %d\n255\n",r,c);
	for(i=0;i<r;i++){
		for(j=0;j<c;j++){
			printf("%d %d %d ",R[i][j],G[i][j],B[i][j]);
		}
	}
}
