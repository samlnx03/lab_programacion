/* Programa: Validar hora */

#include <stdio.h>

int main()
{
    int h, m, s;

    printf( "\n   Introduzca horas: " );
    scanf( "%d", &h );
    printf( "\n   Introduzca minutos: " );
    scanf( "%d", &m );
    printf( "\n   Introduzca segundos: " );
    scanf( "%d", &s );

    if ( h >= 0 && h <= 23){
    	if ( m >= 0 && m <= 59){
				if(s >= 0 && s <= 59){
        			printf( "\n   HORA CORRECTA" );
					return 0;
				}
		}
	}
    printf( "\n   HORA INCORRECTA" );
    return 0;
}
