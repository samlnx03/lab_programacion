#include <stdio.h>

#define NRMAX 100
#define NCMAX 100

float poli(float x);

void ppm_volcado(int r,int c, int R[][NCMAX], int G[][NCMAX], int B[][NCMAX]){
	int i,j;
	printf("P3\n%d %d\n255\n",c,r);
	for (i=0;i<r;i++){
		for(j=0;j<c;j++){
			printf("%d %d %d ",R[i][j], G[i][j], B[i][j]);
		}
		printf("\n");
	}
}

void ppm_blank(int r, int c, int R[][NCMAX], int G[][NCMAX], int B[][NCMAX]){
	int i,j;
	for (i=0;i<r;i++){
		for(j=0;j<c;j++){
			R[i][j]=255;
			G[i][j]=255;
			B[i][j]=255;
		}
	}
}

int main(){

	// la imagen
	int R[NRMAX][NCMAX];
	int G[NRMAX][NCMAX];
	int B[NRMAX][NCMAX];
	int Ren, Col;

	int i=0,j=0;

	// para la funcion del polinomio
	float xmin=-7.0, xmax=14.0;
	float ymin=-340.0, ymax=1270.0;

	float x,y;  // el polinomio

	// para la imagen ppm
	int NC1=0, NC2=NCMAX-1;
	int NR1=0, NR2=NRMAX-1;

	// escalas
	float Ex, Ey;
	
	ppm_blank(NRMAX, NCMAX, R, G, B);
	/*
	x=xmin;
	y=poli(x);
	printf("x:%f, poli(x):%f\n",x,y);
	x=xmax;
	y=poli(x);
	printf("x:%f, poli(x):%f\n",x,y);
	*/

	Ex=(xmax-xmin)/(NC2-NC1);
	Ey=(ymax-ymin)/(NR2-NR1);
	for(x=xmin; x<=xmax; x=x+0.5){
		y=poli(x);
		Col=(x-xmin)/Ex;
		Ren=NR2-(y-ymin)/Ey;
		//printf("x: %f, y:%f, Col:%d, Ren:%d,  RGB: %d-%d-%d\n",x,y,Col,Ren, 
		//	R[Ren][Col], G[Ren][Col], B[Ren][Col]);
		R[Ren][Col]=0;
		G[Ren][Col]=0;
		B[Ren][Col]=0;
		//printf("x: %f, y:%f, Col:%d, Ren:%d,  RGB: %d-%d-%d\n\n",x,y,Col,Ren, 
		//	R[Ren][Col], G[Ren][Col], B[Ren][Col]);
	}
	// eje x
	for(x=xmin; x<=xmax; x=x+0.5){
		y=0;
		Col=(x-xmin)/Ex;
		Ren=NR2-(y-ymin)/Ey;
		R[Ren][Col]=0;
		G[Ren][Col]=0;
		B[Ren][Col]=0;
	}
	// eje y
	for(y=ymin; y<=ymax; y=y+0.5){
		x=0;
		Col=(x-xmin)/Ex;
		Ren=NR2-(y-ymin)/Ey;
		R[Ren][Col]=0;
		G[Ren][Col]=0;
		B[Ren][Col]=0;
	}
	
	ppm_volcado(NRMAX,NCMAX, R, G, B);

}

float poli(float x){
	return x*x*x - 5*(x*x) - 36*x;
}

