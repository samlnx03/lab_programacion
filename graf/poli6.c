// se dibuja 4 funciones
// el area puede ser cualquier region de la imagen

#include <stdio.h>
#include <math.h>

#define NRMAX 1020
#define NCMAX 1020

typedef struct {
	int r1,c1;
	int r2,c2;
	// R1,C1 es la esquina sup izda.   R2,C2 es la esq inf derecha
} rectangulo;

typedef struct {
	unsigned char R,G,B;
} pixel;

//************************* CAMBIO IMPORTANTE
void f1(rectangulo subimg, float xmin, float ymin, float xmax, float ymax, pixel img[][NCMAX]);
void f2(rectangulo subimg, float xmin, float ymin, float xmax, float ymax, pixel img[][NCMAX]);

void ppm_draw_rect(rectangulo R, pixel img[][NCMAX]){
	int i;
	for(i=R.c1; i<=R.c2; i++){
		img[R.r1][i].R=0;
		img[R.r1][i].G=0;
		img[R.r1][i].B=0;

		img[R.r2][i].R=0;
		img[R.r2][i].G=0;
		img[R.r2][i].B=0;
	}

	for(i=R.r1; i<=R.r2; i++){
		img[i][R.c1].R=0;
		img[i][R.c1].G=0;
		img[i][R.c1].B=0;

		img[i][R.c2].R=0;
		img[i][R.c2].G=0;
		img[i][R.c2].B=0;
	}
}
//******************************************

void ejex(rectangulo R, pixel img[][NCMAX], float ymax, float ymin){
	float Ey;
	int Ren;
	int i;
	Ey=(ymax-ymin)/(R.r2-R.r1);
	Ren=R.r2-(0-(ymin))/Ey;
	for(i=R.c1; i<=R.c2; i++){
		img[Ren][i].R=0;
		img[Ren][i].G=0;
		img[Ren][i].B=0;
	}
}
void ejey(rectangulo R, pixel img[][NCMAX], float xmax, float xmin){
	float Ex;
	int Col;
	int i;
	Ex=(xmax-xmin)/(R.c2-R.c1);
	Col=R.c1+(0-(xmin))/Ex;           // CAMBIO IMPORTANTE  ***************************************
	for(i=R.r1; i<=R.r2; i++){
		img[i][Col].R=0;
		img[i][Col].G=0;
		img[i][Col].B=0;
	}
}

float poli(float x);

void ppm_volcado(int r,int c, pixel img[][NCMAX]){
	int i,j;
	printf("P3\n%d %d\n255\n",c,r);
	for (i=0;i<r;i++){
		for(j=0;j<c;j++){
			printf("%d %d %d ",img[i][j].R, img[i][j].G, img[i][j].B);
		}
		printf("\n");
	}
}

void ppm_blank(int r, int c, pixel img[][NCMAX]){
	int i,j;
	for (i=0;i<r;i++){
		for(j=0;j<c;j++){
			img[i][j].R=img[i][j].G=img[i][j].B=255;
		}
	}
}


int main(){

	// la imagen
	pixel img[NRMAX][NCMAX];

	// para la funcion del polinomio
	float xmin=-7.0, xmax=14.0;
	float ymin=-340.0, ymax=1270.0;

	rectangulo rf1={10,10,480,480};
	rectangulo rf2={500,500,999,999};

	ppm_blank(NRMAX, NCMAX, img);

	// para la imagen ppm
	//rectangulo subimg={0,0,NRMAX-1,NCMAX-1};
	f1(rf1, xmin, ymin, xmax, ymax, img);
	f2(rf2, -6.3, -1.2, 6.3, 1.2, img);
	
	//ppm_volcado(NRMAX,NCMAX, R, G, B);
	ppm_volcado(NRMAX,NCMAX, img);

}

void f1(rectangulo subimg, float xmin, float ymin, float xmax, float ymax, pixel img[][NCMAX]){
	int Ren, Col;
	int i=0,j=0;
	float x,y;  // el polinomio
	float Ex, Ey;  // escalas
	
	ppm_draw_rect(subimg, img);
	ejex(subimg, img, ymax, ymin);
	ejey(subimg, img, xmax, xmin);

	Ex=(xmax-xmin)/(subimg.c2-subimg.c1);
	Ey=(ymax-ymin)/(subimg.r2-subimg.r1);

	// evaluar la funcion
	for(x=xmin; x<=xmax; x=x+0.1){
		y=poli(x);
		Col=subimg.c1+(x-xmin)/Ex;           // CAMBIO IMPORTANTE  **************************
		Ren=subimg.r2-(y-ymin)/Ey;

		img[Ren][Col].R=0;
		img[Ren][Col].G=0;
		img[Ren][Col].B=0;
	}

}

float poli(float x){
	return x*x*x - 5*(x*x) - 36*x;
}

// f2-----------------------------------------------------------------------------------------
void f2(rectangulo subimg, float xmin, float ymin, float xmax, float ymax, pixel img[][NCMAX]){
	int Ren, Col;
	int i=0,j=0;
	float x,y;  // el polinomio
	float Ex, Ey;  // escalas
	
	ppm_draw_rect(subimg, img);
	ejex(subimg, img, ymax, ymin);
	ejey(subimg, img, xmax, xmin);

	Ex=(xmax-xmin)/(subimg.c2-subimg.c1);
	Ey=(ymax-ymin)/(subimg.r2-subimg.r1);

	// evaluar la funcion
	for(x=xmin; x<=xmax; x=x+0.01){
		y=sin(x);
		Col=subimg.c1+(x-xmin)/Ex;           // CAMBIO IMPORTANTE  **************************
		Ren=subimg.r2-(y-ymin)/Ey;

		img[Ren][Col].R=0;
		img[Ren][Col].G=0;
		img[Ren][Col].B=0;
	}

}
