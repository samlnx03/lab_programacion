#include <stdio.h>

#define NRMAX 400
#define NCMAX 600

// se corrige el dibujado de los ejes de coordenadas

typedef struct {
			unsigned char R,G,B;
		} pixel;


//************************* CAMBIO IMPORTANTE
void ejex(int NC1, int NC2, int NR1, int NR2, pixel img[][NCMAX], float ymax, float ymin){
	// area de la imagen a usar.
	// NC1, NC2 ancho de la imagen a usar
	// NR1, NR2 alto de la imagen a usar

	// NC1 < NC2   y   NR1 < NR2

	float Ey;
	int Ren;
	int i;
	Ey=(ymax-ymin)/(NR2-NR1);
	Ren=NR2-(0-(ymin))/Ey;
	for(i=NC1; i<=NC2; i++){
		img[Ren][i].R=0;
		img[Ren][i].G=0;
		img[Ren][i].B=0;
	}
}
void ejey(int NC1, int NC2, int NR1, int NR2, pixel img[][NCMAX], float xmax, float xmin){
	float Ex;
	int Col;
	int i;
	Ex=(xmax-xmin)/(NC2-NC1);
	Col=(0-(xmin))/Ex;
	for(i=NR1; i<=NR2; i++){
		img[i][Col].R=0;
		img[i][Col].G=0;
		img[i][Col].B=0;
	}
}
//******************************************

float poli(float x);

void ppm_volcado(int r,int c, pixel img[][NCMAX]){
	int i,j;
	printf("P3\n%d %d\n255\n",c,r);
	for (i=0;i<r;i++){
		for(j=0;j<c;j++){
			printf("%d %d %d ",img[i][j].R, img[i][j].G, img[i][j].B);
		}
		printf("\n");
	}
}

void ppm_blank(int r, int c, pixel img[][NCMAX]){
	int i,j;
	for (i=0;i<r;i++){
		for(j=0;j<c;j++){
			img[i][j].R=img[i][j].G=img[i][j].B=255;
		}
	}
}


int main(){

	// la imagen
	pixel img[NRMAX][NCMAX];

	int Ren, Col;

	int i=0,j=0;

	// para la funcion del polinomio
	float xmin=-7.0, xmax=14.0;
	float ymin=-340.0, ymax=1270.0;

	float x,y;  // el polinomio

	// para la imagen ppm
	int NC1=0, NC2=NCMAX-1;
	int NR1=0, NR2=NRMAX-1;

	// escalas
	float Ex, Ey;
	
	ppm_blank(NRMAX, NCMAX, img);

	Ex=(xmax-xmin)/(NC2-NC1);
	Ey=(ymax-ymin)/(NR2-NR1);

	// evaluar la funcion
	for(x=xmin; x<=xmax; x=x+0.5){
		y=poli(x);
		Col=(x-xmin)/Ex;
		Ren=NR2-(y-ymin)/Ey;

		img[Ren][Col].R=0;
		img[Ren][Col].G=0;
		img[Ren][Col].B=0;
	}

	ejex(NC1, NC2, NR1, NR2, img, ymax, ymin);
	ejey(NC1, NC2, NR1, NR2, img, xmax, xmin);
	
	//ppm_volcado(NRMAX,NCMAX, R, G, B);
	ppm_volcado(NRMAX,NCMAX, img);

}

float poli(float x){
	return x*x*x - 5*(x*x) - 36*x;
}

