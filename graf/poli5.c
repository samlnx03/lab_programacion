// se dibuja un rectangulo alrededor del area donde se grafica el polinomimio
// el area puede ser cualquier region de la imagen

#include <stdio.h>

#define NRMAX 1020
#define NCMAX 1020

typedef struct {
	int r1,c1;
	int r2,c2;
	// R1,C1 es la esquina sup izda.   R2,C2 es la esq inf derecha
} rectangulo;

typedef struct {
	unsigned char R,G,B;
} pixel;

//************************* CAMBIO IMPORTANTE
void ppm_draw_rect(rectangulo R, pixel img[][NCMAX]){
	int i;
	for(i=R.c1; i<=R.c2; i++){
		img[R.r1][i].R=0;
		img[R.r1][i].G=0;
		img[R.r1][i].B=0;

		img[R.r2][i].R=0;
		img[R.r2][i].G=0;
		img[R.r2][i].B=0;
	}

	for(i=R.r1; i<=R.r2; i++){
		img[i][R.c1].R=0;
		img[i][R.c1].G=0;
		img[i][R.c1].B=0;

		img[i][R.c2].R=0;
		img[i][R.c2].G=0;
		img[i][R.c2].B=0;
	}
}
//******************************************

void ejex(rectangulo R, pixel img[][NCMAX], float ymax, float ymin){
	float Ey;
	int Ren;
	int i;
	Ey=(ymax-ymin)/(R.r2-R.r1);
	Ren=R.r2-(0-(ymin))/Ey;
	for(i=R.c1; i<=R.c2; i++){
		img[Ren][i].R=0;
		img[Ren][i].G=0;
		img[Ren][i].B=0;
	}
}
void ejey(rectangulo R, pixel img[][NCMAX], float xmax, float xmin){
	float Ex;
	int Col;
	int i;
	Ex=(xmax-xmin)/(R.c2-R.c1);
	Col=R.c1+(0-(xmin))/Ex;           // CAMBIO IMPORTANTE  ***************************************
	for(i=R.r1; i<=R.r2; i++){
		img[i][Col].R=0;
		img[i][Col].G=0;
		img[i][Col].B=0;
	}
}

float poli(float x);

void ppm_volcado(int r,int c, pixel img[][NCMAX]){
	int i,j;
	printf("P3\n%d %d\n255\n",c,r);
	for (i=0;i<r;i++){
		for(j=0;j<c;j++){
			printf("%d %d %d ",img[i][j].R, img[i][j].G, img[i][j].B);
		}
		printf("\n");
	}
}

void ppm_blank(int r, int c, pixel img[][NCMAX]){
	int i,j;
	for (i=0;i<r;i++){
		for(j=0;j<c;j++){
			img[i][j].R=img[i][j].G=img[i][j].B=255;
		}
	}
}


int main(){

	// la imagen
	pixel img[NRMAX][NCMAX];

	int Ren, Col;

	int i=0,j=0;

	// para la funcion del polinomio
	float xmin=-7.0, xmax=14.0;
	float ymin=-340.0, ymax=1270.0;

	float x,y;  // el polinomio

	// para la imagen ppm
	//rectangulo subimg={0,0,NRMAX-1,NCMAX-1};
	rectangulo subimg={500,500,999,999};

	// escalas
	float Ex, Ey;
	
	ppm_blank(NRMAX, NCMAX, img);

	ppm_draw_rect(subimg, img);

	Ex=(xmax-xmin)/(subimg.c2-subimg.c1);
	Ey=(ymax-ymin)/(subimg.r2-subimg.r1);

	// evaluar la funcion
	for(x=xmin; x<=xmax; x=x+0.5){
		y=poli(x);
		Col=subimg.c1+(x-xmin)/Ex;           // CAMBIO IMPORTANTE  **************************
		Ren=subimg.r2-(y-ymin)/Ey;

		img[Ren][Col].R=0;
		img[Ren][Col].G=0;
		img[Ren][Col].B=0;
	}

	ejex(subimg, img, ymax, ymin);
	ejey(subimg, img, xmax, xmin);
	
	//ppm_volcado(NRMAX,NCMAX, R, G, B);
	ppm_volcado(NRMAX,NCMAX, img);

}

float poli(float x){
	return x*x*x - 5*(x*x) - 36*x;
}

